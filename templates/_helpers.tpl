{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "chart.fullName" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "chart.labels" -}}
helm.sh/chart: {{ include "chart.fullName" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/instance: {{ .Release.Name | quote }}
app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
{{- end }}




{{- define "chart.apm.fullName" -}}
{{ printf "%s-apm" .Release.Name }}
{{- end -}}
{{- define "chart.es.fullName" -}}
{{ printf "%s-es" .Release.Name }}
{{- end -}}
{{- define "chart.kb.fullName" -}}
{{ printf "%s-kb" .Release.Name }}
{{- end -}}
{{- define "chart.ent.fullName" -}}
{{ printf "%s-ent" .Release.Name }}
{{- end -}}
{{- define "chart.ems.fullName" -}}
{{ printf "%s-ems" .Release.Name }}
{{- end -}}
{{- define "chart.epr.fullName" -}}
{{ printf "%s-epr" .Release.Name }}
{{- end -}}
{{- define "chart.ear.fullName" -}}
{{ printf "%s-ear" .Release.Name }}
{{- end -}}
{{- define "chart.esar.fullName" -}}
{{ printf "%s-esar" .Release.Name }}
{{- end -}}
{{- define "chart.fleet.fullName" -}}
{{ printf "%s-fleet" .Release.Name }}
{{- end -}}
{{- define "chart.ingressCertIssuerName" -}}
{{ printf "%s-ingress-issuer" .Release.Name }}
{{- end -}}
{{- define "chart.ingressCertName" -}}
{{ printf "%s-ingress-cert" .Release.Name }}
{{- end -}}
{{- define "chart.ingressSecretReaderRoleName" -}}
{{ printf "%s-reader" (include "chart.ingressCertName" .) }}
{{- end -}}
{{- define "chart.clusterCertName" -}}
{{ printf "%s-cluster-cert" .Release.Name }}
{{- end -}}
{{- define "chart.clusterSecretReaderRoleName" -}}
{{ printf "%s-reader" (include "chart.clusterCertName" .) }}
{{- end -}}
{{- define "chart.clusterSvcFqdn" -}}
{{ printf "%s.svc" .Release.Namespace }}
{{- end -}}


{{- define "chart.apm.svcFullName" -}}
{{ printf "%s-%s" (include "chart.apm.fullName" .) "apm-http" }}
{{- end -}}
{{- define "chart.es.svcFullName" -}}
{{ printf "%s-%s" (include "chart.es.fullName" .) "es-http" }}
{{- end -}}
{{- define "chart.kb.svcFullName" -}}
{{ printf "%s-%s" (include "chart.kb.fullName" .) "kb-http" }}
{{- end -}}
{{- define "chart.ent.svcFullName" -}}
{{ printf "%s-%s" (include "chart.ent.fullName" .) "ent-http" }}
{{- end -}}
{{- define "chart.ems.svcFullName" -}}
{{ printf "%s-%s" (include "chart.ems.fullName" .) "ems-http" }}
{{- end -}}
{{- define "chart.epr.svcFullName" -}}
{{ printf "%s-%s" (include "chart.epr.fullName" .) "epr-http" }}
{{- end -}}
{{- define "chart.ear.svcFullName" -}}
{{ printf "%s-%s" (include "chart.ear.fullName" .) "ear-http" }}
{{- end -}}
{{- define "chart.esar.svcFullName" -}}
{{ printf "%s-%s" (include "chart.esar.fullName" .) "esar-http" }}
{{- end -}}
{{- define "chart.fleet.svcFullName" -}}
{{ printf "%s-%s" (include "chart.fleet.fullName" .) "fleet-agent-http" }}
{{- end -}}


{{- define "chart.apm.ingressFqdn" -}}
{{ printf "%s.%s" "apm" .Values.deploymentDomain }}
{{- end -}}
{{- define "chart.apm.ingressUrl" -}}
{{ printf "https://%s" (include "chart.apm.ingressFqdn" .) }}
{{- end -}}
{{- define "chart.apm.clusterFqdn" -}}
{{ printf "%s.%s" (include "chart.apm.svcFullName" .) (include "chart.clusterSvcFqdn" .) }}
{{- end -}}
{{- define "chart.apm.clusterUrl" -}}
{{ printf "https://%s:%s" (include "chart.apm.clusterFqdn" .) "8200" }}
{{- end -}}

{{- define "chart.es.ingressFqdn" -}}
{{ printf "%s.%s" "es" .Values.deploymentDomain }}
{{- end -}}
{{- define "chart.es.ingressUrl" -}}
{{ printf "https://%s" (include "chart.es.ingressFqdn" .) }}
{{- end -}}
{{- define "chart.es.clusterFqdn" -}}
{{ printf "%s.%s" (include "chart.es.svcFullName" .) (include "chart.clusterSvcFqdn" .) }}
{{- end -}}
{{- define "chart.es.clusterUrl" -}}
{{ printf "https://%s:%s" (include "chart.es.clusterFqdn" .) "9200" }}
{{- end -}}

{{- define "chart.kb.ingressFqdn" -}}
{{ printf "%s.%s" "kb" .Values.deploymentDomain }}
{{- end -}}
{{- define "chart.kb.ingressUrl" -}}
{{ printf "https://%s" (include "chart.kb.ingressFqdn" .) }}
{{- end -}}
{{- define "chart.kb.clusterFqdn" -}}
{{ printf "%s.%s" (include "chart.kb.svcFullName" .) (include "chart.clusterSvcFqdn" .) }}
{{- end -}}
{{- define "chart.kb.clusterUrl" -}}
{{ printf "https://%s:%s" (include "chart.kb.clusterFqdn" .) "5601" }}
{{- end -}}

{{- define "chart.ent.ingressFqdn" -}}
{{ printf "%s.%s" "ent" .Values.deploymentDomain }}
{{- end -}}
{{- define "chart.ent.ingressUrl" -}}
{{ printf "https://%s" (include "chart.ent.ingressFqdn" .) }}
{{- end -}}
{{- define "chart.ent.clusterFqdn" -}}
{{ printf "%s.%s" (include "chart.ent.svcFullName" .) (include "chart.clusterSvcFqdn" .) }}
{{- end -}}
{{- define "chart.ent.clusterUrl" -}}
{{ printf "https://%s:%s" (include "chart.ent.clusterFqdn" .) "3002" }}
{{- end -}}

{{- define "chart.ems.ingressFqdn" -}}
{{ printf "%s.%s" "ems" .Values.deploymentDomain }}
{{- end -}}
{{- define "chart.ems.ingressUrl" -}}
{{ printf "https://%s" (include "chart.ems.ingressFqdn" .) }}
{{- end -}}
{{- define "chart.ems.clusterFqdn" -}}
{{ printf "%s.%s" (include "chart.ems.svcFullName" .) (include "chart.clusterSvcFqdn" .) }}
{{- end -}}
{{- define "chart.ems.clusterUrl" -}}
{{ printf "https://%s:%s" (include "chart.ems.clusterFqdn" .) "8080" }}
{{- end -}}

{{- define "chart.epr.ingressFqdn" -}}
{{ printf "%s.%s" "epr" .Values.deploymentDomain }}
{{- end -}}
{{- define "chart.epr.ingressUrl" -}}
{{ printf "https://%s" (include "chart.epr.ingressFqdn" .) }}
{{- end -}}
{{- define "chart.epr.clusterFqdn" -}}
{{ printf "%s.%s" (include "chart.epr.svcFullName" .) (include "chart.clusterSvcFqdn" .) }}
{{- end -}}
{{- define "chart.epr.clusterUrl" -}}
{{ printf "https://%s:%s" (include "chart.epr.clusterFqdn" .) "443" }}
{{- end -}}

{{- define "chart.ear.ingressFqdn" -}}
{{ printf "%s.%s" "ear" .Values.deploymentDomain }}
{{- end -}}
{{- define "chart.ear.ingressUrl" -}}
{{ printf "https://%s" (include "chart.ear.ingressFqdn" .) }}
{{- end -}}
{{- define "chart.ear.clusterFqdn" -}}
{{ printf "%s.%s" (include "chart.ear.svcFullName" .) (include "chart.clusterSvcFqdn" .) }}
{{- end -}}
{{- define "chart.ear.clusterUrl" -}}
{{ printf "https://%s:%s" (include "chart.ear.clusterFqdn" .) "80" }}
{{- end -}}

{{- define "chart.esar.ingressFqdn" -}}
{{ printf "%s.%s" "esar" .Values.deploymentDomain }}
{{- end -}}
{{- define "chart.esar.ingressUrl" -}}
{{ printf "https://%s" (include "chart.esar.ingressFqdn" .) }}
{{- end -}}
{{- define "chart.esar.clusterFqdn" -}}
{{ printf "%s.%s" (include "chart.esar.svcFullName" .) (include "chart.clusterSvcFqdn" .) }}
{{- end -}}
{{- define "chart.esar.clusterUrl" -}}
{{ printf "https://%s:%s" (include "chart.esar.clusterFqdn" .) "80" }}
{{- end -}}

{{- define "chart.fleet.ingressFqdn" -}}
{{ printf "%s.%s" "fleet" .Values.deploymentDomain }}
{{- end -}}
{{- define "chart.fleet.ingressUrl" -}}
{{ printf "https://%s" (include "chart.fleet.ingressFqdn" .) }}
{{- end -}}
{{- define "chart.fleet.clusterFqdn" -}}
{{ printf "%s.%s" (include "chart.fleet.svcFullName" .) (include "chart.clusterSvcFqdn" .) }}
{{- end -}}
{{- define "chart.fleet.clusterUrl" -}}
{{ printf "https://%s:%s" (include "chart.fleet.clusterFqdn" .) "8220" }}
{{- end -}}
